import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './admin/components/dashboard/dashboard.component';
import { AdminComponent } from './admin/components/admin/admin.component';
import { TaskListComponent } from './admin/components/task-list/task-list.component';
import { UserListComponent } from './admin/components/user-list/user-list.component';
import { UserFormComponent } from './admin/components/user-form/user-form.component';
import { TaskFormComponent } from './admin/components/task-form/task-form.component';

const routes: Routes = [
  {
    path: "admin", 
    component: AdminComponent,
    children: [
      {path: "", component: DashboardComponent},
      {path: "task-list", component: TaskListComponent},
      {path: "task-form", component: TaskFormComponent},
      {path: "task-form/:id", component: TaskFormComponent},
      {path: "user-list", component: UserListComponent},
      {path: "user-form", component: UserFormComponent},
      {path: "user-form/:id", component: UserFormComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
