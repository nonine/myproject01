import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { User } from 'src/app/core/api/user';
import { Users } from 'src/app/core/models/exampleUsers';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogBoxComponent } from '../../../core/components/confirm-dialog-box/confirm-dialog-box.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements AfterViewInit{
  private UserList: User[] = Users;
  public displayedColumns: string[] = ['id', 'name', 'surname','age', 'date', 'actions'];
  public dataSource: MatTableDataSource<User>;

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  
  public constructor(public dialog: MatDialog){
    this.dataSource = new MatTableDataSource(this.UserList);
  }
  public ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  private deleteUser(user: User){
    this.UserList = this.UserList.filter(e=>e!==user);
    console.log(this.UserList);
    this.dataSource = new MatTableDataSource<User>(this.UserList);
  }
  public openDialogForDelete(info: User): void {
    let dialogRef = this.dialog.open(ConfirmDialogBoxComponent,{
      data: {name: info.name, surname: info.surname}
    });
    dialogRef.afterClosed().subscribe(result=>{
      if(result){
        this.deleteUser(info)
      }
    })
  }
}
