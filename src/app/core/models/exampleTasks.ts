import { Task } from "../api/task";

export const Tasks: Task[] = [
    {
        id: 1,
        name: "Przykładowe zadanie",
        date: new Date(1995, 8, 1),
    },
    {
        id: 2,
        name: "Jakieś tam zadanie",
        date: new Date(2001, 12, 13),
    },
    {
        id: 3,
        name: "Iść na siłownie",
        date: new Date(2002, 4, 20),
    }
];