import { Component, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Task } from 'src/app/core/api/task';
import { Tasks } from 'src/app/core/models/exampleTasks';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogBoxComponent } from '../../../core/components/confirm-dialog-box/confirm-dialog-box.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent {
  private TaskList: Task[] = Tasks;
  public displayedColumns: string[] = ['id', 'name', 'date', 'actions'];
  public dataSource: MatTableDataSource<Task>;
  
  @ViewChild(MatSort) sort: MatSort = new MatSort();
  
  public constructor(public dialog: MatDialog){
    this.dataSource = new MatTableDataSource(this.TaskList);
  }
  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public deleteTask(task: Task): void{
    this.TaskList = this.TaskList.filter(e=>e!==task);
    console.log(this.TaskList);
    this.dataSource = new MatTableDataSource<Task>(this.TaskList);
  }
  public openDialogForDelete(info: Task): void {
    let dialogRef = this.dialog.open(ConfirmDialogBoxComponent,{
      data: {name: info.name}
    });
    dialogRef.afterClosed().subscribe(result=>{
      console.log(result);
      if(result){
        this.deleteTask(info);
      }
    })
  }
}
