import { User } from "../api/user";

export const Users: User[] = [
    {
        id: 1,
        name: "Piwo",
        surname: "Piwowarski",
        age: 12,
        date: new Date(2022, 2, 13)
    },
    {
        id: 2,
        name: "Wojciech",
        surname: "Szwed",
        age: 30,
        date: new Date(2022, 6, 3)
    },
    {
        id: 3,
        name: "Zbigniew",
        surname: "Kowal",
        age: 12,
        date: new Date(2023, 8, 23)
    }
];