import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent {
  public taskForm: FormGroup;
  public constructor(private router: Router ) {
    this.taskForm = new FormGroup({
      taskName: new FormControl("", [Validators.required, Validators.minLength(3)])
    });
  }
  public onSubmit(){
    console.log(this.taskForm);
  }
  public titleChange(){
    if(this.router.url.match("/admin/task-form/")){
      return $localize`Edytowanie`;
    }else{
      return $localize`Dodawanie`;
    }
  }
  public taskNameError(){
    const taskNameControl = this.taskForm.get('taskName');

    if(taskNameControl?.hasError('required')){
      return $localize`Zadanie musi być podane`;
    }else if(taskNameControl?.hasError('minlength')){
      return $localize`Zadanie musi mieć conajmniej 3 znaki`;
    }

    return false;
  }
}
