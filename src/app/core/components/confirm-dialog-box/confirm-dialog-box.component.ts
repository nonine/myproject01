import { Component,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-dialog-box',
  templateUrl: './confirm-dialog-box.component.html',
  styleUrls: ['./confirm-dialog-box.component.scss']
})
export class ConfirmDialogBoxComponent {
  constructor(public dialogRef: MatDialogRef<ConfirmDialogBoxComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private router: Router) {
  }
  public titleChange(){
    if(this.router.url.match("/admin/user-list")){
      return $localize`użytkownika`;
    }else{
      return $localize`zadania`;
    }
  }
}
