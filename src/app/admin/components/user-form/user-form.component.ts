import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent{
  public userForm: FormGroup;
  public constructor(private router: Router ) {
    this.titleChange();
    this.userForm = new FormGroup({
      userName: new FormControl("", [Validators.required, Validators.minLength(3),  Validators.pattern("^[a-zA-ZąęćżźóńłśĄĘĆŻŹÓŃŁŚ]*$")]),
      userSurname: new FormControl("", [Validators.required, Validators.minLength(3), Validators.pattern("^[a-zA-ZąęćżźóńłśĄĘĆŻŹÓŃŁŚ]*$")]),
      userAge: new FormControl("", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.max(120), Validators.min(3)])
    });
  }
  public titleChange(){
    if(this.router.url.match("/admin/user-form/")){
      return $localize`Edytowanie`;
    }else{
      return $localize`Dodawanie`;
    }
  }
  public onSubmit(){
    console.log(this.userForm);
  }
  public userNameError(){
    if(this.userForm.controls["userName"].errors?.['required']){
      return $localize`Imię musi być podane`;
    }else if(this.userForm.controls["userName"].errors?.['minlength']){
      return $localize`Imię musi mieć conajmniej 3 litery`;
    }
    return false;
  }
  public userSurnameError(){;
    if(this.userForm.controls["userSurname"].errors?.['required']){
      return $localize`Nazwisko musi być podane`;
    }else if(this.userForm.controls["userSurname"].errors?.['minlength']){
      return $localize`Nazwisko musi mieć conajmniej 3 litery`;
    }
    return false;
  }
  public userAgeError(){
    if(this.userForm.controls['userAge'].errors?.['required']){
      return $localize`Wiek musi być podany`;
    }else if(this.userForm.controls['userAge'].errors?.['pattern']){
      return $localize`Wiek musi być liczbą`
    }else if(this.userForm.controls['userAge'].errors?.['min']){
      return $localize`Jesteś za młody`;
    }else if(this.userForm.controls['userAge'].errors?.['max']){
      return $localize`Jesteś za stary`;
    }
    return false;
  }
}
